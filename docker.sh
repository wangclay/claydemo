#!/bin/sh
# 定义应用组名
group_name='clay'
# 定义应用名称
app_name=${DRONE_REPO_NAME}
# 定义应用版本
app_version=${DRONE_COMMIT}
echo ${app_version}
# 打包编译docker镜像
echo '----build image start----'
docker build -t ${group_name}/${app_name} .
echo '----build image success----'
docker tag ${group_name}/${app_name} $REGISTRY/$REGISTRY_NAMESPACE/${app_name}:${app_version}
docker push $REGISTRY/$REGISTRY_NAMESPACE/${app_name}:${app_version}
echo 'push success'