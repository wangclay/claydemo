FROM openjdk:8u332-jdk

ENV SERVICE_PORTS=8080

RUN mkdir -p /app/

WORKDIR /app

COPY ./start.sh /app/
COPY ./.dockerignore /app/
COPY ./claydemo-0.0.1.jar /app/

RUN chmod 755 -R /app/


ENTRYPOINT ["/app/start.sh"]