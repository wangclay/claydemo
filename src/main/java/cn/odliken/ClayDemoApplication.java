package cn.odliken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClayDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClayDemoApplication.class,args);
    }
}
